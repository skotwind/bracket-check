import os

cmd = {
    'packet delete': 'sudo pip3 uninstall bracket_check -y',
    'packet sdist': 'python3 setup.py sdist',
    'packet install': 'sudo python3 setup.py install',
    'packet check': 'bracket_check',
}

answers = {
    1: "\33[32mOK\33[39m",
    0: "\33[31mNOT\33[39m",
}


def conclusion(cmd_text: str, status: bool):
    print(f'Performing "{cmd_text}": {answers[status]}')
    if not status:
        raise AssertionError


def enter_cmd(cmd_dict: dict):
    try:
        for text, command in cmd_dict.items():
            conclusion(text, os.system(command) == 0)
    except AssertionError:
        pass
    else:
        print(f"It is ready to use: {answers[True]}")


def run():
    enter_cmd(cmd)
