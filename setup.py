try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

from bracket_check import __version__

setup(name='bracket_check',
      version=__version__,
      packages=['bracket_check', 'bracket_check.app'],
      entry_points={
          'console_scripts': [
              'bracket_check = bracket_check:main',
          ]
      },
      )